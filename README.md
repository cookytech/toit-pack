# Toit Pack
Internal utility package, built at Cookytech Technologies to help us build our products. 
Will cover things from driver libraries to utilities to helpers within one import package.


## Drivers:

### AHTXX Temperature and Humidity i2C Sensors
Drivers tested for high usage stability with inbuilt soft reset and recovery.
Works with these sensors (Indian store links):
* (AHT 10)[https://robu.in/product/aht10-high-precision-digital-temperature-and-humidity-measurement-module/]
* (AHT 25)[https://robu.in/product/temperature-and-humidity-sensor-model-aht25/]

```
import toit-pack

main:
  sensor := toit-pack.AHTTemperatureHumiditySensor --scl=22 --sda=21
  while true:
    reading := sensor.measure
    print "$reading.temperature °C"
    print "$reading.humidity %"
```

### Capacitive Moisture Sensor
Driver tested to work with capacitive moisture sensor. Supports both raw reading and inverse-normalized output for better and more accurate % value

(Indian Store link):
* (Capacitive Moisture Sensor V1.2)[https://robu.in/product/capacitive-soil-moisture-sensor-v2-0/]
```
import toit-pack

main:
  cms := toit-pack.CapacitiveMoistureSensor 33
  // pin := gpio.Pin 33
  // adc := adc.Adc pin
  100.repeat: 
    sleep --ms=1500
    print "absolute: $cms.reading"
    print "normalized: $cms.percentage%"

```

### (coming soon) Resistive Soil Moisture Sensor
Driver for resistive soil moiture sensor with an added persistence driven modification for self-calibration.


## Features and bugs

Please file feature requests and bugs at the [issue tracker][tracker].

[tracker]: https://gitlab.com/cookytech/toit-pack/-/issues
