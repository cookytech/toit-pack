/*
Author: <Raveesh Agarwal> (raveesh@cookytech.in)
ahtXX.toit (c) 2023
Desc: Driver for AHT Temperature and Pressure Sensors
Created:  2023-09-20T14:07:07.884Z
Modified: !date!
*/

import gpio
import i2c
import math show pow

// This driver is confirmed to be working properly on AHT 10 and AHT 25

class AHTTemperatureHumiditySensor:
  bus /i2c.Bus := ? 
  sda_ /int := ?
  scl_ /int := ?
  sda_pin_ /gpio.Pin := ?
  scl_pin_ /gpio.Pin := ?
  device_ /i2c.Device := ?

  constructor --scl /int --sda /int:
    sda_ = sda
    scl_ = scl
    sda_pin_ = gpio.Pin sda 
    scl_pin_ = gpio.Pin scl
    bus = i2c.Bus 
      --sda=sda-pin_ 
      --scl=scl-pin_
    device_ = bus.device 0x38
  
  close_:
    device_.close
    bus.close
    sda-pin_.close
    scl-pin_.close

  open_ :
    sda_pin_ = gpio.Pin sda_ 
    scl_pin_ = gpio.Pin scl_
    bus = i2c.Bus 
      --sda=sda-pin_ 
      --scl=scl-pin_
    device_ = bus.device 0x38

  soft-reset_:
    device_.write #[0b10111010]
  
  measure -> AHTXXReading:
    measurement /AHTXXReading? := null 
    exception := catch: 
      /* 
      After power-on, wait no less than 100ms.  
      Before reading the temperature and humidity value, get a byte of statusword by sending 0x71. 
      If the status word and 0x18 are not equal to 0x18, initialize the 0x1B, 0x1C, 0x1Eregisters, 
      details Please refer to our official website routine for the initialization process;
      if they are equal, proceed to thenext
      */
      // wait for 100ms to for the sensor to start
      sleep --ms=100

      /**
      Scan and prints I2C device addresses for during debugging
      */
      devices := bus.scan
      print "Devices: $devices"

      /**
      Write to the device to get the status reading
      */
      device_.write #[0x71]
      status_reading := device_.read 1
      if status_reading[0] == 0x18:
        print "AHT-XX: Device is ready, Sending read signal."
        /**
        Wait 10ms to send the 0xAC command (trigger measurement). This command parameter has two bytes, thefirst
        byte is 0x33, and the second byte is 0x00.
        */
        sleep --ms=12
        device_.write #[0xAC, 0x33, 0x00]
        /**
        Give 80ms to the sensor to measure. If the data is still not available, wait another 10ms and read the status
        */
        is-read-ready := false
        is-calibrated := false
        sleep --ms=80
        while is-read-ready == false:
          sleep --ms=10
          status-reading = device_.read 1
          is_read_ready = status-reading[0] & 0b00000001 == 0
          print "AHT-XX: read_ready"
        is_calibrated = status-reading[0] & 0b00010000 == 1
        data_reading := device_.read 7
        measurement = AHTXXReading --reading=data_reading
        print "AHT-25: Temperature: $measurement.temperature.stringify, Humidity: $measurement.humidity.stringify"
        soft-reset_
      else:
        print "Sensor not ready, performing a soft reset"
        soft-reset_
        // TODO: check whether the soft reset worked, and do a hard reset: initialize the 0x1B, 0x1C, 0x1E registers" 
        sleep --ms=100                
        measurement = measure // again
    if exception != null or measurement == null:
        print "Exception: $exception.stringify"
        close_
        open_
        soft-reset_
        measurement =  measure
        return measurement
    else :
      sleep --ms=50
      return measurement

/**
Class for parsing binary stream to human-understandable readings
abstracted away from the sensor command sequence for better distrubition of responsibilities
*/      
class AHTXXReading:
  temperature /float := 0.0
  humidity /float := 0.0
  divisor_ ::= pow 2 20

  get-temperature-from-byte-array_ --reading /ByteArray -> float:
    temp := (reading[3] & 0b00001111) << 16 | reading[4] << 8 | reading[5]
    return temp / divisor_ * 200.0 - 50

  get-humidity-from-byte-array_ --reading /ByteArray -> float:
    hum := reading[1] << 12 | reading[2] << 4 | reading[3] & 0b11110000 >> 4
    return hum / divisor_ * 100

  constructor --reading /ByteArray:
    temperature = get-temperature-from-byte-array_ --reading=reading
    humidity = get-humidity-from-byte-array_ --reading=reading