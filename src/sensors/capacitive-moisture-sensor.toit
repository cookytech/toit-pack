import gpio
import gpio.adc

class CapacitiveMoistureSensor:
  pin-number := null
  pin_ := null
  adc_ := null

  min-reading_ := null
  max-reading_ := null

  reading_ := null
  
  constructor .pin-number:
    pin_ = gpio.Pin pin-number
    adc_ = adc.Adc pin_
    reading_ = adc_.get
    min-reading_ = reading_
    max-reading_ = reading_
  
  qualify_ reading:
    if reading > max-reading_:
      max-reading_ = reading
    else if reading < min-reading_:
      min-reading_ = reading

  reading:
    reading_ = adc_.get
    qualify_ reading_
    return reading_
  
  normalized_:
    //inverse normalization yields more accurate calibration with respect to intermediary states
    return 100 * (1/reading_ - 1/max-reading_) / (1/min-reading_ - 1/max-reading_)

  percentage:
    reading_ = adc_.get
    qualify_ reading_
    return normalized_.to-int

