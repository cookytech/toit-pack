import toit-pack

main:
  cms := toit-pack.CapacitiveMoistureSensor 33
  // pin := gpio.Pin 33
  // adc := adc.Adc pin
  100.repeat: 
    sleep --ms=1500
    print "absolute: $cms.reading"
    print "normalized: $cms.percentage%"
