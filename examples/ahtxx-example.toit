import toit-pack

main:
  sensor := toit-pack.AHTTemperatureHumiditySensor --scl=22 --sda=21
  while true:
    reading := sensor.measure
    print "$reading.temperature °C"
    print "$reading.humidity %"

